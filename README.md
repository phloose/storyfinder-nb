# StoryFinder Notebooks

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/phloose%2Fstoryfinder-nb/master?urlpath=lab%2Ftree%2Fnotebooks)

A collection of jupyter notebooks illustrating the geovisual analytics workflow for the
algorithms implemented in [StoryFinder
Analytics](https://gitlab.com/phloose/storyfinder-analytics).

It is strongly recommended to use python in a dedicated conda environment (e.g.
[Miniconda](https://docs.conda.io/en/latest/miniconda.html)). You can use the prepared
`environment.yml` in the binder directory of this
repository. It creates a new environment and installs necessary dependencies:

`conda env create -f binder/environment.yml`

When finished run

`conda activate sfnb`

to activate the environment. To start a notebook or lab instance run:

`jupyter notebook notebooks/<notebook>.ipynb` or `jupyter lab notebooks/<notebook>.ipynb`

The notebooks make use of [ipyleaflet](https://ipyleaflet.readthedocs.io/en/latest/).
When using [JupyterLab](https://jupyterlab.readthedocs.io/en/stable/) you need to
install a specific extension:

`jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyter-leaflet`
