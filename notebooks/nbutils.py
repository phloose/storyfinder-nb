from ipywidgets import Layout
from matplotlib.colors import DivergingNorm

maplayout = Layout(width="100%", height="540px")


def plot(gdf, attr, size=(15, 15)):
    """Plot the z- or p-value of a Hotspot analysis"""
    vmin, vmax = getattr(gdf, attr).min(), getattr(gdf, attr).max()
    norm = DivergingNorm(vmin=vmin, vcenter=0, vmax=vmax)

    gdf.plot(
        column=attr,
        cmap="bwr",
        edgecolor="lightgrey",
        linewidth=0.2,
        legend=True,
        norm=norm,
        figsize=size,
    )
